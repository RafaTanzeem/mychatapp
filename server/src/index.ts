import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import usersRouter from "./routes/user";
import db from "./config/database";
import friendRouter from "./routes/friend";
import chatRouter from "./routes/chatRoom";
import messageRoute from "./routes/message";
import log from "./utils/logger";
import swaggerDocs from "./utils/swagger";
import http from "http";
import { Server } from "socket.io";

dotenv.config();
const app = express();

var corsOptions = {
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

db.sync()
  .then(() => {
    log.info("connected database");
  })
  .catch((err) => log.error(err));

app.use("/user", usersRouter);
app.use("/friend", friendRouter);
app.use("/chat", chatRouter);
app.use("/message", messageRoute);

const port: number = 8081;

const server = http.createServer(app);

const io = new Server(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket) => {
  console.log("user Connected", socket.id);

  socket.on("disconnect", () => {
    console.log("user disconnected", socket.id);
  });
});

server.listen(port, () => {
  log.info(`server started at ${port}...`);
  swaggerDocs(app, port);
});
