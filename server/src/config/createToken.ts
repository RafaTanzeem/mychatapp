import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();

export const createToken = (id: number) => {
  return jwt.sign({ id }, process.env.SECRET as string, {
    expiresIn: 3600,
  });
};
