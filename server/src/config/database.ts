import { Sequelize } from "sequelize-typescript";
import dotenv from "dotenv";
import User from "../models/user";
import ChatRoom from "../models/chatRooms";
import Message from "../models/messages";
import Friend from "../models/friend";
import Group from "../models/Group";
dotenv.config();

console.log(process.env.HOST);

const db = new Sequelize({
  dialect: "mysql",
  username: process.env.USERNAME,
  host: process.env.HOST,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  models: [User, ChatRoom, Message, Friend, Group],
});

export default db;
