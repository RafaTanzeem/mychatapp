import { Router } from "express";
import {
  getUsers,
  getUserById,
  registerUser,
  loginUser,
} from "../controllers/user";
import { verifyToken } from "../middlewares/authorization";
import {
  checkDuplicateUsernameOrEmail,
  validateInputs,
} from "../middlewares/validations";

const router = Router();

router.post(
  "/register",
  checkDuplicateUsernameOrEmail,
  validateInputs,
  registerUser
);
router.post("/login", loginUser);
router.get("/", verifyToken, getUsers);
router.get("/:id", verifyToken, getUserById);

export default router;

/**
 * @openapi
 * '/user/register':
 *  post:
 *     tags:
 *     - User
 *     summary: Register a user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateUserInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CreateUserResponse'
 *      409:
 *        description: Conflict
 *      422:
 *        description: Bad request
 *      500:
 *        description: server not responding
 */

/**
 * @openapi
 * '/user/login':
 *  post:
 *     tags:
 *     - User
 *     summary: ligin user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/LogUserInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/LogUserResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      500:
 *        description: server not responding
 */

/**
 * @openapi
 * '/user':
 *  get:
 *     tags:
 *      - User
 *     summary: Users List
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CreateUserResponse'
 *      500:
 *        description: server not responding
 */

/**
 * @openapi
 * '/user/{id}':
 *  get:
 *     tags:
 *      - User
 *     summary: Users by ID
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *       - in: path
 *         name: id
 *         description: User ID
 *         required: true
 *         schema:
 *           type: number
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CreateUserResponse'
 *      404:
 *        description: user not found
 *      500:
 *        description: server not responding
 */
