import { Router } from "express";
import {
  acceptFriendRequest,
  createFriendREquest,
  getFriends,
} from "../controllers/friend";
import { verifyToken } from "../middlewares/authorization";

const router = Router();

router.get("/", verifyToken, getFriends);
router.post("/", verifyToken, createFriendREquest);
router.put("/", verifyToken, acceptFriendRequest);

export default router;
/**
 * @openapi
 * '/friend':
 *  get:
 *     tags:
 *     - Friends
 *     summary: get friends list
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/friendsGetResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      409:
 *        description: Forbidden
 *      500:
 *        description: server not responding
 *
 *  post:
 *     tags:
 *     - Friends
 *     summary: Request a friend
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/friendRequestInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/friendRequestResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      409:
 *        description: Forbidden
 *      500:
 *        description: server not responding
 *
 *  put:
 *     tags:
 *     - Friends
 *     summary: Accept a friend
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/acceptRequestInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/friendRequestResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      409:
 *        description: Forbidden
 *      500:
 *        description: server not responding
 */
