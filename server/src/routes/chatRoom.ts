import { Router } from "express";
import { createChatRoom, getChatById, getChats } from "../controllers/chatRoom";

import { verifyToken } from "../middlewares/authorization";

const router = Router();

router.get("/", verifyToken, getChats);
router.post("/", verifyToken, createChatRoom);
router.get("/:id", verifyToken, getChatById);

export default router;

/**
 * @openapi
 * '/chat':
 *  post:
 *     tags:
 *     - ChatRoom
 *     summary: Create chatRoom
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/chatInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/chatResponse'
 *      409:
 *        description: Conflict
 *      422:
 *        description: Bad request
 *      500:
 *        description: server not responding
 *
 *  get:
 *     tags:
 *      - ChatRoom
 *     summary: Chat List
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/chatListResponse'
 *      500:
 *        description: server not responding
 */

/**
 * @openapi
 * '/chat/{id}':
 *  get:
 *     tags:
 *      - ChatRoom
 *     summary: Chat room by ID
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *       - in: path
 *         name: id
 *         description: Chat ID
 *         required: true
 *         schema:
 *           type: number
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/chatIdResponse'
 *      404:
 *        description: chat not found
 *      500:
 *        description: server not responding
 */
