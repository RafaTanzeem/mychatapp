import { Router } from "express";
import { createMessage } from "../controllers/message";
import { verifyToken } from "../middlewares/authorization";

const router = Router();

router.post("/", verifyToken, createMessage);

export default router;

/**
 * @openapi
 * '/message':
 *  post:
 *     tags:
 *     - Message
 *     summary: send message
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/messageInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/messageResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      500:
 *        description: server not responding
 */
