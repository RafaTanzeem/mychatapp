import { Optional } from "sequelize";
import { Table, Model, Column, ForeignKey } from "sequelize-typescript";
import ChatRoom from "./chatRooms";
import User from "./user";

interface GroupAttributes {
  userId: number;
  chatId: number;
}

interface GroupCreationAttributes extends Optional<GroupAttributes, "chatId"> {}

@Table({
  timestamps: true,
  tableName: "Groups",
})
class Group extends Model<GroupAttributes, GroupCreationAttributes> {
  @ForeignKey(() => User)
  @Column
  userId!: number;

  @ForeignKey(() => ChatRoom)
  @Column
  chatId!: number;

  static createGroup = async (chatRoom: ChatRoom, users: number[]) => {
    for (let i = 0; i < users.length; i++) {
      await Group.create({
        userId: users[i],
        chatId: chatRoom.id,
      });
    }
  };
}

export default Group;
