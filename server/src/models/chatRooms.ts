import { Optional } from "sequelize";
import {
  Table,
  Model,
  Column,
  DataType,
  HasMany,
  BelongsToMany,
} from "sequelize-typescript";
import Group from "./Group";
import Message from "./messages";
import User from "./user";

interface ChatAttributes {
  id: number;
  name: string;
  desc: string;
}

interface ChatCreationAttributes extends Optional<ChatAttributes, "id"> {}

@Table({
  timestamps: true,
  tableName: "ChatRooms",
})
class ChatRoom extends Model<ChatAttributes, ChatCreationAttributes> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name!: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  desc!: string;

  @HasMany(() => Message)
  messages?: Message[];

  @BelongsToMany(() => User, () => Group)
  users?: User[];
}

export default ChatRoom;

/**
 * @openapi
 * components:
 *  schemas:
 *    chatInput:
 *      type: object
 *      required:
 *        - name
 *        - users
 *      properties:
 *        name:
 *          type: string
 *          default: MyChat
 *        desc:
 *          type: string
 *        users:
 *          type: array
 *          items:
 *            type: number
 *          default: [1]
 *    chatResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        data:
 *          type: object
 *          properties:
 *            name:
 *              type: string
 *            desc:
 *              type: string
 *            updatedAt:
 *              type: string
 *            createdAt:
 *              type: string
 *    chatListResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        data:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              id:
 *                type: number
 *              name:
 *                type: string
 *              desc:
 *                type: string
 *              updatedAt:
 *                type: string
 *              createdAt:
 *                type: string
 *    chatIdResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        data:
 *          type: object
 *          properties:
 *           chat:
 *            type: object
 *
 */
