import { Optional } from "sequelize";
import {
  Table,
  Model,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
} from "sequelize-typescript";
import ChatRoom from "./chatRooms";
import User from "./user";

interface MessageAttributes {
  userId: string;
  content: string;
  chatId: number;
}

interface MessageCreationAttributes
  extends Optional<MessageAttributes, "content"> {}

@Table({
  timestamps: true,
  tableName: "Messages",
})
class Message extends Model<MessageAttributes, MessageCreationAttributes> {
  @ForeignKey(() => User)
  @Column
  userId!: number;
  @BelongsTo(() => User)
  user!: User;

  @ForeignKey(() => ChatRoom)
  @Column
  chatId!: number;
  @BelongsTo(() => ChatRoom)
  chatRoom!: ChatRoom;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  content!: string;
}

export default Message;

/**
 * @openapi
 * components:
 *  schemas:
 *    messageInput:
 *      type: object
 *      required:
 *        - chatId
 *        - content
 *      properties:
 *        chatId:
 *          type: number
 *          default: 1
 *        content:
 *          type: string
 *          default: Hello
 *    messageResponse:
 *      type: object
 *      properties:
 *        userId:
 *          type: number
 *        chatId:
 *          type: number
 *        content:
 *          type: string
 *
 */
