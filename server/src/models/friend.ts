import { Optional } from "sequelize";
import { Table, Model, Column, DataType, Default } from "sequelize-typescript";

interface GroupAttributes {
  id: number;
  user_1: number;
  user_2: number;
  status: boolean;
}

interface GroupCreationAttributes extends Optional<GroupAttributes, "id"> {}

@Table({
  timestamps: true,
  tableName: "Friends",
})
class Friend extends Model<GroupAttributes, GroupCreationAttributes> {
  @Column({
    type: DataType.BIGINT,
    allowNull: false,
  })
  user_1!: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  user_2!: number;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: true,
  })
  status!: boolean;
}

export default Friend;

/**
 * @openapi
 * components:
 *  schemas:
 *    friendsGetResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *        data:
 *          type: array
 *          items:
 *            type: object
 *    friendRequestInput:
 *      type: object
 *      properties:
 *        id:
 *          type: number
 *          default: 1
 *    friendRequestResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *    acceptRequestInput:
 *      type: object
 *      properties:
 *        id:
 *          type: number
 *        status:
 *          type: boolean
 */
