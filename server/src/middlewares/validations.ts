import { Request, Response, NextFunction } from "express";
import User from "../models/user";
import schema from "../config/validations";

export const validateInputs = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = schema.validate(req.body);
  if (error == null || 0) {
    next();
  } else {
    const { details } = error;
    const message = details.map((i) => i.message).join(",");

    console.log("error", message);
    res.status(422).json({ error: message });
  }
};

export const checkDuplicateUsernameOrEmail = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Username
  let user = await User.findOne({
    where: {
      name: req.body.name,
    },
  });
  if (user) {
    return res.status(409).send({
      message: "Failed! Username is already in use!",
    });
  }

  // Email
  user = await User.findOne({
    where: {
      email: req.body.email,
    },
  });
  if (user) {
    res.status(409).send({
      message: "Failed! Email is already in use!",
    });
    return;
  }

  next();
};
