import jwt, { JsonWebTokenError, JwtPayload } from "jsonwebtoken";
import { NextFunction, Request, Response } from "express";
import dotenv from "dotenv";
import log from "../utils/logger";
dotenv.config();

export const verifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let token: string;
  token = req.headers["x-access-token"] as string;

  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }
  interface JwtPayload {
    id: number;
  }

  try {
    const decoded = jwt.verify(
      token,
      process.env.SECRET as string
    ) as JwtPayload;
    req.body._id = decoded.id;
    log.info(`"Token Verify By User with id:${decoded.id} "`);

    next();
  } catch (err) {
    res.send({ message: "unauthorizes", error: err });
  }
};

// const catchError = (err: JsonWebTokenError, res: Response) => {
//   if (err instanceof JsonWebTokenError) {
//     return res
//       .status(401)
//       .send({ message: "Unauthorized! Access Token was expired!" });
//   }

//   return res.sendStatus(401).send({ message: "Unauthorized!" });
// };
