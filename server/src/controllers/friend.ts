import { Request, Response } from "express";
import { Op } from "sequelize";
import Friend from "../models/friend";
import User from "../models/user";

export const createFriendREquest = async (req: Request, res: Response) => {
  try {
    await Friend.create({
      user_1: req.body._id,
      user_2: req.body.id,
      status: false,
    });
    res.status(200).send({ message: "Friend Request sent successfully" });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

export const acceptFriendRequest = async (req: Request, res: Response) => {
  try {
    await Friend.update(
      { status: true },
      { where: { user_2: req.body._id, user_1: req.body.id } }
    );
    res.status(200).send({ message: "Friend Request Accepted" });
  } catch (err) {
    res
      .status(500)
      .send({ message: "server error something went wrong", error: err });
  }
};

export const getFriends = async (req: Request, res: Response) => {
  try {
    const friends: Friend[] = await Friend.findAll({
      where: {
        [Op.or]: [{ user_2: req.body._id }, { user_1: req.body._id }],
        status: true,
      },
    });

    const friendsId = friends.map((v) => {
      if (req.body._id !== v.user_1) return v.user_1;
      else return v.user_2;
    });
    const users = await User.findAll({ where: { id: friendsId } });
    res.status(200).send({ message: "Success", data: users });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};
