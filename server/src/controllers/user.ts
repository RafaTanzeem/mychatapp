import { Request, Response } from "express";
import User from "../models/user";
import bcrypt from "bcrypt";
import { createToken } from "../config/createToken";

const registerUser = async (req: Request, res: Response) => {
  try {
    const user = await User.create({
      name: req.body.name,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
    });
    res.send({ message: "User was registered successfully!", data: user });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

const loginUser = async (req: Request, res: Response) => {
  console.log(req.body);
  try {
    const user = await User.findOne({
      where: {
        email: req.body.email,
      },
    });
    if (!user) {
      return res.status(404).send({ message: "User Not found." });
    }

    let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

    if (!passwordIsValid) {
      return res.status(401).send({
        accessToken: null,
        message: "Unauthorised, Invalid Password!",
      });
    }
    let token: string = createToken(user.id);
    res.status(200).send({
      message: "User LogedIn Successfully",
      data: { id: user.id, name: user.name, email: user.email },
      accessToken: token,
    });
  } catch (err) {
    res
      .status(500)
      .send({ status: 500, message: "Internal server error", errors: err });
  }
};
const getUsers = async (req: Request, res: Response) => {
  try {
    const users: User[] = await User.findAll();
    res.status(200).send({ data: users });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};
const getUserById = async (req: Request, res: Response) => {
  try {
    const user: null | User = await User.findByPk(req.params.id);
    if (!user) res.status(404).send({ message: "user not found" });
    res.status(200).send({ data: user });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};

export { registerUser, loginUser, getUsers, getUserById };
