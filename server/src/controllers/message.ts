import { Request, Response } from "express";
import Message from "../models/messages";

export const createMessage = async (req: Request, res: Response) => {
  console.log("message", req.body);
  try {
    const message = await Message.create({
      userId: req.body._id,
      chatId: req.body.chatId,
      content: req.body.content,
    });
    res
      .status(200)
      .send({ message: "message sent successfully", data: message });
  } catch (err) {
    res.status(500).send({ message: err });
  }
};
