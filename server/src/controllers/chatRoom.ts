import { Request, Response } from "express";
import ChatRoom from "../models/chatRooms";
import Group from "../models/Group";
import Message from "../models/messages";

export const createChatRoom = async (req: Request, res: Response) => {
  try {
    const chat = await ChatRoom.create({
      name: req.body.name,
      desc: req.body.desc,
    });
    const users: number[] = req.body.users;
    users.push(req.body._id);
    await Group.createGroup(chat, users);
    res.status(200).send({ message: "Chat Room created", data: chat });
  } catch (err) {
    res.status(500).send({ message: "server not responding", error: err });
  }
};

export const getChats = async (req: Request, res: Response) => {
  try {
    const chatsOfUsre = await Group.findAll({
      where: { userId: req.body._id },
    });
    const chatIds: number[] = [];
    chatsOfUsre.map((group) => {
      if (chatIds.includes(group.chatId)) return;
      chatIds.push(group.chatId);
    });
    const chat = await ChatRoom.findAll({
      where: { id: chatIds },
    });
    console.log(chat[0].users);
    res.status(200).send({ message: "Your Chat Rooms List", data: chat });
  } catch (err) {
    res.status(500).send({ message: "server not responding", error: err });
  }
};

export const getChatById = async (req: Request, res: Response) => {
  console.log("id", req.params.id);
  try {
    const chat = await ChatRoom.findOne({
      where: { id: req.params.id },
      include: [Message],
    });
    if (!chat)
      res.status(404).send({ message: "no caht room found with that id" });
    res.status(200).send({ message: "Chat Room Opened", data: chat });
  } catch (err) {
    res.status(500).send({ message: "server not responding", error: err });
  }
};
