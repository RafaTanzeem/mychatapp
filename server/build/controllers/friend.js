"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFriends = exports.acceptFriendRequest = exports.createFriendREquest = void 0;
const sequelize_1 = require("sequelize");
const friend_1 = __importDefault(require("../models/friend"));
const user_3 = __importDefault(require("../models/user"));
const createFriendREquest = async (req, res) => {
    try {
        const request = await friend_1.default.create({
            user_1: req.body._id,
            user_2: req.body.id,
            status: false,
        });
        res.status(200).send({ message: "Friend Request sent successfully" });
    }
    catch (err) {
        res.status(500).send({ message: err });
    }
};
exports.createFriendREquest = createFriendREquest;
const acceptFriendRequest = async (req, res) => {
    try {
        const request = await friend_1.default.update({ status: true }, { where: { user_2: req.body._id, user_1: req.body.id } });
        res.status(200).send({ message: "Friend Request Accepted" });
    }
    catch (err) {
        res
            .status(500)
            .send({ message: "server error something went wrong", error: err });
    }
};
exports.acceptFriendRequest = acceptFriendRequest;
const getFriends = async (req, res) => {
    try {
        const friends = await friend_1.default.findAll({
            where: {
                [sequelize_1.Op.or]: [{ user_2: req.body._id }, { user_1: req.body._id }],
                status: true,
            },
        });
        const friendsId = friends.map((v) => {
            if (req.body._id !== v.user_1)
                return v.user_1;
            else
                return v.user_2;
        });
        const users = await user_3.default.findAll({ where: { id: friendsId } });
        res.status(200).send({ message: "Success", data: users });
    }
    catch (err) {
        res.status(500).send({ message: err });
    }
};
exports.getFriends = getFriends;
