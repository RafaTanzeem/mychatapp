"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.getUserById = exports.getUsers = exports.loginUser = exports.registerUser = void 0;
const user_1 = __importDefault(require("../models/user"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const createToken_1 = require("../config/createToken");
const registerUser = async (req, res) => {
    console.log(req.body);
    try {
        const user = await user_1.default.create({
            name: req.body.name,
            email: req.body.email,
            password: bcrypt_1.default.hashSync(req.body.password, 8),
        });
        res.send({ message: "User was registered successfully!", data: user });
    }
    catch (err) {
        res.status(500).send({ message: err });
    }
};
exports.registerUser = registerUser;
const loginUser = async (req, res) => {
    console.log(req.body);
    try {
        const user = await user_1.default.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (!user) {
            return res.status(404).send({ message: "User Not found." });
        }
        let passwordIsValid = bcrypt_1.default.compareSync(req.body.password, user.password);
        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: "Unauthorised, Invalid Password!",
            });
        }
        let token = (0, createToken_1.createToken)(user.id);
        // req.header(token);
        res.status(200).send({
            message: "User LogedIn Successfully",
            accessToken: token,
        });
    }
    catch (err) {
        res
            .status(500)
            .send({ status: 500, message: "Internal server error", errors: err });
    }
};
exports.loginUser = loginUser;
const getUsers = async (req, res) => {
    console.log(req.body._id);
    try {
        const users = await user_1.default.findAll();
        res.status(200).send({ data: users });
    }
    catch (err) {
        res.status(500).send({ message: err });
    }
};
exports.getUsers = getUsers;
const getUserById = () => { };
exports.getUserById = getUserById;
const deleteUser = () => { };
exports.deleteUser = deleteUser;
