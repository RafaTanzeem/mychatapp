"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const message_1 = require("../controllers/message");
const authorization_1 = require("../middlewares/authorization");
const router = (0, express_1.Router)();
router.post("/", authorization_1.verifyToken, message_1.createMessage);
exports.default = router;
