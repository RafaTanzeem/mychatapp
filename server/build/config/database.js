"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
const dotenv_1 = __importDefault(require("dotenv"));
const user_1 = __importDefault(require("../models/user"));
const chatRooms_1 = __importDefault(require("../models/chatRooms"));
const messages_1 = __importDefault(require("../models/messages"));
const friend_1 = __importDefault(require("../models/friend"));
const Group_1 = __importDefault(require("../models/Group"));
dotenv_1.default.config();
console.log(process.env.HOST);
const db = new sequelize_typescript_1.Sequelize({
    dialect: "mysql",
    username: process.env.USERNAME,
    host: process.env.HOST,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    models: [user_1.default, chatRooms_1.default, messages_1.default, friend_1.default, Group_1.default],
});
exports.default = db;
