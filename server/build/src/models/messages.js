"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
const chatRooms_1 = __importDefault(require("./chatRooms"));
const user_1 = __importDefault(require("./user"));
let Message = class Message extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => user_1.default),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Message.prototype, "userId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => user_1.default),
    __metadata("design:type", user_1.default)
], Message.prototype, "user", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => chatRooms_1.default),
    sequelize_typescript_1.Column,
    __metadata("design:type", Number)
], Message.prototype, "chatId", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => chatRooms_1.default),
    __metadata("design:type", chatRooms_1.default)
], Message.prototype, "chatRoom", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: false,
    }),
    __metadata("design:type", String)
], Message.prototype, "content", void 0);
Message = __decorate([
    (0, sequelize_typescript_1.Table)({
        timestamps: true,
        tableName: "Messages",
    })
], Message);
exports.default = Message;
// /**
//  * @openapi
//  * components:
//  *  schemas:
//  *    messageInput:
//  *      type: object
//  *      required:
//  *        - chatId
//  *        - content
//  *      properties:
//  *        chatId:
//  *          type: number
//  *          default: 1
//  *        content:
//  *          type: string
//  *          default: Hello
//  *    messageResponse:
//  *      type: object
//  *      properties:
//  *        userId:
//  *          type: number
//  *        chatId:
//  *          type: number
//  *        content:
//  *          type: string
//  *
//  */
