"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const message_1 = require("../controllers/message");
const authorization_1 = require("../middlewares/authorization");
const router = (0, express_1.Router)();
router.post("/", authorization_1.verifyToken, message_1.createMessage);
exports.default = router;
/**
 * @openapi
 * '/message':
 *  post:
 *     tags:
 *     - Message
 *     summary: send message
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/messageInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/messageResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      500:
 *        description: server not responding
 */
