"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const friend_1 = require("../controllers/friend");
const authorization_1 = require("../middlewares/authorization");
const router = (0, express_1.Router)();
router.get("/", authorization_1.verifyToken, friend_1.getFriends);
router.post("/", authorization_1.verifyToken, friend_1.createFriendREquest);
router.put("/", authorization_1.verifyToken, friend_1.acceptFriendRequest);
exports.default = router;
/**
 * @openapi
 * '/friend':
 *  get:
 *     tags:
 *     - Friends
 *     summary: get friends list
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/friendsGetResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      409:
 *        description: Forbidden
 *      500:
 *        description: server not responding
 *
 *  post:
 *     tags:
 *     - Friends
 *     summary: Request a friend
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/friendRequestInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/friendRequestResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      409:
 *        description: Forbidden
 *      500:
 *        description: server not responding
 *
 *  put:
 *     tags:
 *     - Friends
 *     summary: Accept a friend
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/acceptRequestInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/friendRequestResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      409:
 *        description: Forbidden
 *      500:
 *        description: server not responding
 */
