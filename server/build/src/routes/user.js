"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = require("../controllers/user");
const authorization_1 = require("../middlewares/authorization");
const validations_1 = require("../middlewares/validations");
const router = (0, express_1.Router)();
router.post("/register", validations_1.checkDuplicateUsernameOrEmail, validations_1.validateInputs, user_1.registerUser);
router.post("/login", user_1.loginUser);
router.get("/", authorization_1.verifyToken, user_1.getUsers);
router.get("/:id", authorization_1.verifyToken, user_1.getUserById);
exports.default = router;
/**
 * @openapi
 * '/user/register':
 *  post:
 *     tags:
 *     - User
 *     summary: Register a user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateUserInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CreateUserResponse'
 *      409:
 *        description: Conflict
 *      422:
 *        description: Bad request
 *      500:
 *        description: server not responding
 */
/**
 * @openapi
 * '/user/login':
 *  post:
 *     tags:
 *     - User
 *     summary: ligin user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/LogUserInput'
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/LogUserResponse'
 *      404:
 *        description: Not Found
 *      401:
 *        description: Unauthorised
 *      500:
 *        description: server not responding
 */
/**
 * @openapi
 * '/user':
 *  get:
 *     tags:
 *      - User
 *     summary: Users List
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CreateUserResponse'
 *      500:
 *        description: server not responding
 */
/**
 * @openapi
 * '/user/{id}':
 *  get:
 *     tags:
 *      - User
 *     summary: Users by ID
 *     parameters:
 *       - in: header
 *         name: x-access-token
 *         schema:
 *           type: string
 *           format: jwt
 *         required: true
 *       - in: path
 *         name: id
 *         description: User ID
 *         required: true
 *         schema:
 *           type: number
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CreateUserResponse'
 *      404:
 *        description: user not found
 *      500:
 *        description: server not responding
 */
