"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
const user_1 = __importDefault(require("./routes/user"));
const database_1 = __importDefault(require("./config/database"));
const friend_1 = __importDefault(require("./routes/friend"));
const chatRoom_1 = __importDefault(require("./routes/chatRoom"));
const message_1 = __importDefault(require("./routes/message"));
const logger_1 = __importDefault(require("./utils/logger"));
const swagger_1 = __importDefault(require("./utils/swagger"));
// import http from "http";
// import { Server } from "socket.io";
dotenv_1.default.config();
const app = (0, express_1.default)();
var corsOptions = {
    origin: "http://localhost:3000",
};
app.use((0, cors_1.default)(corsOptions));
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
database_1.default.sync()
    .then(() => {
    logger_1.default.info("connected database");
})
    .catch((err) => logger_1.default.error(err));
app.use("/user", user_1.default);
app.use("/friend", friend_1.default);
app.use("/chat", chatRoom_1.default);
app.use("/message", message_1.default);
const port = 8081;
// const server = http.createServer(app);
// const io = new Server(server);
app.listen(port, () => {
    logger_1.default.info(`server started at ${port}...`);
    (0, swagger_1.default)(app, port);
});
