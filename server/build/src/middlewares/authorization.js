"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
const logger_1 = __importDefault(require("../utils/logger"));
dotenv_1.default.config();
const verifyToken = (req, res, next) => {
    let token;
    logger_1.default.info("header:", req.headers);
    token = req.headers["x-access-token"];
    if (!token) {
        return res.status(403).send({ message: "No token provided!" });
    }
    try {
        const decoded = jsonwebtoken_1.default.verify(token, process.env.SECRET);
        req.body._id = decoded.id;
        logger_1.default.info(`"Token Verify By User with id:${decoded.id} "`);
        next();
    }
    catch (err) {
        res.send({ message: "unauthorizes", error: err });
    }
};
exports.verifyToken = verifyToken;
// const catchError = (err: JsonWebTokenError, res: Response) => {
//   if (err instanceof JsonWebTokenError) {
//     return res
//       .status(401)
//       .send({ message: "Unauthorized! Access Token was expired!" });
//   }
//   return res.sendStatus(401).send({ message: "Unauthorized!" });
// };
