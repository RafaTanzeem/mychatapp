"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMessage = void 0;
const messages_1 = __importDefault(require("../models/messages"));
const createMessage = async (req, res) => {
    try {
        const message = await messages_1.default.create({
            userId: req.body._id,
            chatId: req.body.chatId,
            content: req.body.content,
        });
        res
            .sendStatus(200)
            .json({ message: "message sent successfully", data: message });
    }
    catch (err) {
        res.sendStatus(500).json({ message: "server not responding", error: err });
    }
};
exports.createMessage = createMessage;
