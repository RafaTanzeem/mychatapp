"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChatById = exports.getChats = exports.createChatRoom = void 0;
const chatRooms_1 = __importDefault(require("../models/chatRooms"));
const Group_1 = __importDefault(require("../models/Group"));
const messages_1 = __importDefault(require("../models/messages"));
const createChatRoom = async (req, res) => {
    try {
        const chat = await chatRooms_1.default.create({
            name: req.body.name,
            desc: req.body.desc,
        });
        const users = req.body.users;
        users.push(req.body._id);
        await Group_1.default.createGroup(chat, users);
        res.status(200).send({ message: "Chat Room created", data: chat });
    }
    catch (err) {
        res.status(500).send({ message: "server not responding", error: err });
    }
};
exports.createChatRoom = createChatRoom;
const getChats = async (req, res) => {
    try {
        const chatsOfUsre = await Group_1.default.findAll({
            where: { userId: req.body._id },
        });
        const chatIds = [];
        chatsOfUsre.map((group) => {
            if (chatIds.includes(group.chatId))
                return;
            chatIds.push(group.chatId);
        });
        const chat = await chatRooms_1.default.findAll({
            where: { id: chatIds },
        });
        console.log(chat[0].users);
        res.status(200).send({ message: "Your Chat Rooms List", data: chat });
    }
    catch (err) {
        res.status(500).send({ message: "server not responding", error: err });
    }
};
exports.getChats = getChats;
const getChatById = async (req, res) => {
    console.log("id", req.params.id);
    try {
        const chat = await chatRooms_1.default.findOne({
            where: { id: req.params.id },
            include: [messages_1.default],
        });
        if (!chat)
            res.status(404).send({ message: "no caht room found with that id" });
        res.status(200).send({ message: "Chat Room Opened", data: chat });
    }
    catch (err) {
        res.status(500).send({ message: "server not responding", error: err });
    }
};
exports.getChatById = getChatById;
