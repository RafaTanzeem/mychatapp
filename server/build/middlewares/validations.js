"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkDuplicateUsernameOrEmail = exports.validateInputs = void 0;
const user_1 = __importDefault(require("../models/user"));
const validations_1 = __importDefault(require("../config/validations"));
const validateInputs = async (req, res, next) => {
    const { error } = validations_1.default.validate(req.body);
    if (error == null || 0) {
        next();
    }
    else {
        const { details } = error;
        const message = details.map((i) => i.message).join(",");
        console.log("error", message);
        res.status(422).json({ error: message });
    }
};
exports.validateInputs = validateInputs;
const checkDuplicateUsernameOrEmail = async (req, res, next) => {
    // Username
    let user = await user_1.default.findOne({
        where: {
            name: req.body.name,
        },
    });
    if (user) {
        return res.status(400).send({
            message: "Failed! Username is already in use!",
        });
    }
    // Email
    user = await user_1.default.findOne({
        where: {
            email: req.body.email,
        },
    });
    if (user) {
        res.status(400).send({
            message: "Failed! Email is already in use!",
        });
        return;
    }
    next();
};
exports.checkDuplicateUsernameOrEmail = checkDuplicateUsernameOrEmail;
