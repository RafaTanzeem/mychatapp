import "./App.css";
import { Routes, Route, Link } from "react-router-dom";
import UsersList from "./screens/UsersList";
import Home from "./screens/Home";
import ChatRoom from "./screens/ChatRoom";
import ChatList from "./screens/ChatList";
import Login from "./screens/Login";
import Register from "./screens/Register";

function App() {
  return (
    <div className="App">
      <Link to="/">
        <h1>chat app</h1>
      </Link>
      <Link to="/login">
        <button>LogIn</button>
      </Link>
      <Link to="/register">
        <button>Regiter</button>
      </Link>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/users" element={<UsersList />} />
        <Route path="/chatRoom" element={<ChatRoom />} />
        <Route path="/chatList" element={<ChatList />} />
      </Routes>
    </div>
  );
}

export default App;
