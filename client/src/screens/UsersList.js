import React, { useEffect, useState } from "react";
import axios from "axios";

export default function UsersList() {
  const [data, setData] = useState([]);
  const getData = async () => {
    const res = await axios.get(`http://localhost:8081/user`, {
      headers: {
        "x-access-token": `${localStorage.getItem("accessToken")}`,
      },
    });

    setData(res.data.data);
  };
  useEffect(() => {
    getData();
  }, []);

  console.log(data);

  const sendRequest = async (id) => {
    try {
      const res = await axios.post(
        "http://localhost:8081/friend",

        {
          id: id,
        },
        {
          headers: {
            "x-access-token": `${localStorage.getItem("accessToken")}`,
          },
        }
      );
      alert(res.data.message);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      {Array.isArray(data) ? (
        data.map((user) => (
          <div key={user.id}>
            <h1>User Name: {user.name}</h1>
            <h2>Email: {user.email} </h2>
            <button type="button" onClick={() => sendRequest(user.id)}>
              Request
            </button>
          </div>
        ))
      ) : (
        <p>Login Again </p>
      )}
    </>
  );
}
