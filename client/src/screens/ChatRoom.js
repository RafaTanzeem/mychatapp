import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";

export default function ChatRoom() {
  const location = useLocation();
  const { id } = location.state;
  console.log("cahtId:", id);

  const [data, setData] = useState({});
  const [message, setMessage] = useState("");
  const getData = async () => {
    const res = await axios.get(`http://localhost:8081/chat/${id}`, {
      headers: {
        "x-access-token": `${localStorage.getItem("accessToken")}`,
      },
    });
    console.log("res", res);
    setData(res.data.data.messages);
  };

  useEffect(() => {
    console.log("useffect");
    getData();
  }, []);

  console.log(data);

  const handelClick = (e) => {
    e.preventDefault();
    axios.post(
      `http://localhost:8081/message`,
      {
        content: message,
        chatId: id,
      },
      {
        headers: {
          "x-access-token": `${localStorage.getItem("accessToken")}`,
        },
      }
    );
  };

  return (
    <div>
      <div>
        {Array.isArray(data) ? (
          data.map((msg) => {
            return <li key={msg.id}>{msg.content}</li>;
          })
        ) : (
          <p>Not Array </p>
        )}
      </div>
      <div>
        <input
          type="text"
          value={message}
          placeholder="write message..."
          onChange={(e) => {
            console.log(e.target.value);
            return setMessage(e.target.value);
          }}
        ></input>
        <button type="button" onClick={handelClick}>
          &#9658;
        </button>
      </div>
    </div>
  );
}
