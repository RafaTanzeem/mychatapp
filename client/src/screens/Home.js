import React from "react";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div>
      <h1>Home</h1>
      <Link to={"/users"}>
        <button type="button">UsersList</button>
      </Link>
      <Link to={"/chatli"}>
        <button>ChatRoomList</button>
      </Link>
    </div>
  );
}
