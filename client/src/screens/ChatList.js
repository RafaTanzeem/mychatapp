import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import io from "socket.io-client";
let socket;

const ChatList = () => {
  const [data, setData] = useState([]);
  const getData = async () => {
    const res = await axios.get(`http://localhost:8081/chat`, {
      headers: {
        "x-access-token": `${localStorage.getItem("accessToken")}`,
      },
    });

    setData(res.data.data);
  };
  useEffect(() => {
    getData();
    socket = io.connect("http://localhost:8081");
  }, []);

  console.log(data);
  return (
    <div>
      {data.map((chat) => {
        return (
          <div key={chat.id}>
            <Link to="/chatRoom" state={{ id: chat.id, socket: socket }}>
              <h1>
                {chat.id}:{chat.name}
              </h1>
            </Link>
            <h2>{chat.desc}</h2>
          </div>
        );
      })}
    </div>
  );
};

export default ChatList;
